# Tarea 1: Predictores de saltos

## Descripción

El programa simula los siguientes predictores de saltos:

- Bimodal

- PShare

- GShare

- Tournament

Este permite obtener métricas de rendimiento utilizando como entrada un trace
donde se indica la dirección del salto así como el resultado de este.

## Prerrequisitos

```c
gzip
```

## Utilización

Para compilar, se utiliza:

```c
make build
```

Para correr el programa, se utiliza:

```c
gunzip -c branch-trace-gcc.trace.gz | ./branch -s < # > -bp < # > -gh < # > -ph < # > -o < # >
```

## Notas

### Acerca del parámetro de entrada bp
Para el parámetro bp, los predictores de saltos se identifican de la siguiente manera:

0 -> Bimodal

1 -> PShare

2 -> GShare

3 -> Tournament

### Acerca de la información desplegada en consola
Cada espacio representa:

- ```Number of correct prediction of taken branches```: Número de predicciones Taken que coinciden con el outcome.

- ```Number of incorrect prediction of taken branches```: Número de predicciones Taken que no coinciden con el outcome.

- ```Correct prediction of not taken branches```: Número de predicciones Not Taken que coinciden con el outcome.

- ```Incorrect prediction of not taken branches```: Número de predicciones Not Taken que no coinciden con el outcome.
