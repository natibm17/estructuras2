/**
 * file tournament.c
 * author Natalia Bolaños Murillo
 * Logic for Tournament prediction
 **/

 // INCLUDES
#include "tournament.h"

int metapredictor(int* meta, individual_result* gshare, individual_result* ph, long index)
{
    int prediction;

    // Get state
    int counter = meta[index];

    // If the state is 00 or 01, prediction is Not Taken
    if (counter == 0 || counter == 1)
    {
        prediction = 0;
    }

    // If the state is 10 or 11, prediction is Taken
    else {
        prediction = 1;
    }

    // If outcome is taken, state is increased by one
    if ((ph->correct == 1) && (gshare->correct == 0))
    {
        if (meta[index] != 3)
        {
            meta[index] = meta[index] + 1;
        }
    }

    // If outcome is not taken, state is decreased by one
    else if ((ph->correct == 0) && (gshare->correct == 1) )
    {
        if (meta[index] != 0)
        {
            meta[index] = meta[index] - 1;
        }
    }
    else{
    }
    return prediction;
}

individual_result* tournament(int* meta, int* BHT, int* BHT2, unsigned long* PHT, unsigned long PC,
    char* outcome, int s, int ph, int gh, general_result* gen_result, unsigned long* history)
{
    // Get index
    unsigned long index = get_index(PC, s);

    // Get pshare and gshare predictions
    individual_result* ph_result = pshare(BHT, PHT, PC, outcome, s, ph, gen_result);
    individual_result* gh_result = gshare(BHT2, PC, outcome, s, gh, gen_result, history);
    individual_result* temp_result = create_individual_result();

    // Get metapredictor prediction
    int prediction = metapredictor(meta, gh_result, ph_result, index);

    // If the metapredictor selects pshare
    if (prediction == 1)
    {
        ph_result->predictor = 1;

        // Add to counters if prediction was correct taken or correct not taken
        if(ph_result->correct == 1)
        {
            if(strcmp(ph_result->prediction, "T") == 0)
            {
                gen_result->correct_taken_t++;
            }
            else
            {
                gen_result->correct_not_taken_t++;
            }
        }

        // Add to counters if prediction was incorrect taken or incorrect not taken
        else
        {
            if(strcmp(ph_result->prediction, "T") == 0)
            {
                gen_result->incorrect_taken_t++;
            }
            else
            {
                gen_result->incorrect_not_taken_t++;
            }
        }
        temp_result =  ph_result;
    }

    // If the metapredictor selects gshare
    else if (prediction == 0)
    {
        gh_result->predictor = 1;

        // Add to counters if prediction was correct taken or correct not taken
        if(gh_result->correct == 1)
        {
            if(strcmp(gh_result->prediction, "T") == 0)
            {
                gen_result->correct_taken_t++;
            }
            else
            {
                gen_result->correct_not_taken_t++;
            }
        }

        // Add to counters if prediction was incorrect taken or incorrect not taken
        else
        {
            if(strcmp(gh_result->prediction, "T") == 0)
            {
                gen_result->incorrect_taken_t++;
            }
            else
            {
                gen_result->incorrect_not_taken_t++;
            }
        }
        gh_result->predictor = 0;
        temp_result = gh_result;
    }
    return temp_result;
}
