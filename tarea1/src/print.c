/**
 * file print.c
 * author Natalia Bolaños Murillo
 * Logic for Printing results
 **/

// INCLUDES
#include "print.h"

void print_results(general_result* results, int bp)
{
    // Calculate final results
    get_results(results, bp);

    // If prediction type is tournament
    if (bp == 3)
    {
        printf("%s\n","--------------------------------------------------------------------" );
        printf("%s\n","Prediction parameters:" );
        printf("%s\n","--------------------------------------------------------------------" );
        printf("%s %s\n","Branch prediction type:                             ", results->prediction_type );
        printf("%s %ld\n","BHT size (entries):                                 ", results->bht_size );
        printf("%s %i\n","Global history register size:                       ", results->gh_size );
        printf("%s %i\n","Private history register size:                      ", results->ph_size );
        printf("%s\n","--------------------------------------------------------------------" );
        printf("%s\n","Simulation results: ");
        printf("%s\n","--------------------------------------------------------------------" );
        printf("%s %i\n","Number of branch:                                   ", results->total_branch);
        printf("%s %i\n","Number of correct prediction of taken branches:     ", results->correct_taken_t);
        printf("%s %i\n","Number of incorrect prediction of taken branches:   ", results->incorrect_taken_t);
        printf("%s %i\n","Correct prediction of not taken branches:           ", results->correct_not_taken_t);
        printf("%s %i\n","Incorrect prediction of not taken branches:         ", results->incorrect_not_taken_t);
        printf("%s %f%s\n","Percentage of correct predictions:                  ", results->percentage, "%");
        printf("%s\n","--------------------------------------------------------------------" );
    }

    // If prediction type is bimodal, pshare or gshare
    else
    {
        printf("%s\n","--------------------------------------------------------------------" );
        printf("%s\n","Prediction parameters:" );
        printf("%s\n","--------------------------------------------------------------------" );
        printf("%s %s\n","Branch prediction type:                             ", results->prediction_type );
        printf("%s %ld\n","BHT size (entries):                                 ", results->bht_size );
        printf("%s %i\n","Global history register size:                       ", results->gh_size );
        printf("%s %i\n","Private history register size:                      ", results->ph_size );
        printf("%s\n","--------------------------------------------------------------------" );
        printf("%s\n","Simulation results: ");
        printf("%s\n","--------------------------------------------------------------------" );
        printf("%s %i\n","Number of branch:                                   ", results->total_branch);
        printf("%s %i\n","Number of correct prediction of taken branches:     ", results->correct_taken);
        printf("%s %i\n","Number of incorrect prediction of taken branches:   ", results->incorrect_taken);
        printf("%s %i\n","Correct prediction of not taken branches:           ", results->correct_not_taken);
        printf("%s %i\n","Incorrect prediction of not taken branches:         ", results->incorrect_not_taken);
        printf("%s %f%s\n","Percentage of correct predictions:                  ", results->percentage, "%");
        printf("%s\n","--------------------------------------------------------------------" );
    }
}

void get_results(general_result* results, int bp)
{
    // If prediction type is tournament
    if(bp == 3)
    {
        results->total_branch = results->correct_taken_t + results->incorrect_taken_t +
        results->correct_not_taken_t + results->incorrect_not_taken_t;
        results->percentage = ((double)(results->correct_taken_t + results->correct_not_taken_t)/(double)results->total_branch)*100.0;

    }

    // If prediction type is bimodal, pshare or gshare
    else
    {
        results->total_branch = results->correct_taken + results->incorrect_taken +
        results->correct_not_taken + results->incorrect_not_taken;
        results->percentage = ((double)(results->correct_taken + results->correct_not_taken)/(double)results->total_branch)*100.0;
    }
}

void create_file(char* filename, int bp)
{
    FILE *f = fopen(filename, "w");
    if (f == NULL)
    {
        printf("Error opening file en create file!\n");
        exit(1);
    }

    // Print results on file
    const char *text;

    // If prediction type is tournament
    if (bp == 3)
    {
        text = "PC              Predictor       Outcome         Prediction          correct/incorrect";
    }
    else
    {
        text = "PC              Outcome      Prediction     correct/incorrect";
    }
    fprintf(f, "%s\n", text);
    fclose(f);
}

void add_line(individual_result* results, char* filename, int bp)
{
    FILE *f = fopen(filename, "a");
    const char* predictor = "P";
    if (f == NULL)
    {
        printf("Error opening file en add_line!\n");
        exit(1);
    }

    const char* correct;
    if (results->correct == 1)
    {
        correct = "correct";
    }
    else
    {
        correct = "incorrect";
    }

    // If prediction type is tournament
    if (bp == 3)
    {
        if (results->predictor == 0)
        {
            predictor = "G";
        }
        fprintf(f, "%lu      %s               %s                  %s                 %s\n", results->instruction, predictor,
            results->outcome, results->prediction, correct);
    }

    // If prediction type is bimodal, pshare or gshare
    else{
        fprintf(f, "%lu      %s            %s              %s\n", results->instruction,
            results->outcome, results->prediction, correct);
    }
    fclose(f);
}
