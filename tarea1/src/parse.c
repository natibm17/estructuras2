/**
 * file parse.c
 * author Natalia Bolaños Murillo
 * Logic for Parsing the file
 **/

 // INCLUDES
#include "parse.h"

void parse(FILE* file, int s, int bp, int gh, int ph, int o)
{
    if(file == NULL){
        exit(1);
    }
    else
    {
        // Create BHT, PHT and metapredictor table
        long bht_size = pow(2,(double)s);
        int* BHT = malloc((int)bht_size*sizeof(int));
        int* BHT2 = malloc((int)bht_size*sizeof(int));
        int* meta = malloc((int)bht_size*sizeof(int));
        unsigned long* PHT = malloc((int)bht_size*sizeof(unsigned long));

        // General results struct is created
        general_result* gen_result = create_general_result();

        // All tables are initialized with the desired values
        for(int i=0; i<bht_size ; i++)
        {
            BHT[i] = 0;
            BHT2[i] = 0;
            PHT[i] = 0;
            meta[i] = 3;
        }

        // gshare history register is created
        unsigned long history_gh = 0;

        // General results are stored into struct
        gen_result->bht_size = bht_size;
        gen_result->ph_size = ph;
        gen_result->gh_size = gh;

        // Prediction type is selected depending on input bp
        if (bp == 0)
        {
            gen_result->prediction_type = "Bimodal";
        }
        // gshare
        else if (bp == 1)
        {
            gen_result->prediction_type = "Pshare";
        }
        //pshare
        else if (bp == 2)
        {
            gen_result->prediction_type = "Gshare";
        }
        // tournament
        else if (bp == 3)
        {
            gen_result->prediction_type = "Tournament";
        }
        // results file is creted
        char* filename = malloc(15*sizeof(char));
        filename = strcat(strcat(filename, gen_result->prediction_type), ".txt");
        if (o)
        {
            create_file(filename, bp);
        }

        // Individual branch results struct is created
        individual_result* temp_result;

        int c = 0;
        char line[100];
        // Read one line of the create_file
        while (fgets(line, 100, file) != NULL)
        {
            // Data is read from the line
            line_data* data = analize_line(line);

            // Predictor is selected depending on input bp
            // Bimodal
            if (bp == 0)
            {
                temp_result = bimodal(BHT, data->instruction, data->outcome, s, gen_result);
            }
            // Pshare
            else if (bp == 1)
            {
                temp_result = pshare(BHT, PHT, data->instruction, data->outcome, s, ph, gen_result);
            }
            // Gshare
            else if (bp == 2)
            {
                temp_result = gshare(BHT, data->instruction, data->outcome, s, gh, gen_result, &history_gh);
            }
            // Tournament
            else if (bp == 3)
            {
                temp_result = tournament(meta, BHT, BHT2, PHT, data->instruction, data->outcome, s, ph, gh,
                     gen_result, &history_gh);
            }

            // First 5000 results are printed on txt file if o=1
            if(o == 1 && c < 5000)
            {
               add_line(temp_result, filename, bp);
               c=c+1;
            }
        }

        // Results are printed on terminal if o=1

        print_results(gen_result, bp);


        // free memory
        free(BHT);
        free(BHT2);
        free(PHT);
        free(meta);
        free(filename);
        free(gen_result);
        free(temp_result);
        fclose(file);
    }
}

line_data* create_line_data()
{
    line_data* data = malloc(sizeof(line_data));
    data->instruction = 0;
    data->outcome = malloc(BUFFER_SIZE*sizeof(char));
    return data;
}

line_data* analize_line(char* line)
{
    line_data* data = create_line_data();
    sscanf(line, "%lu %s", &data->instruction, data->outcome);
    return data;
}
