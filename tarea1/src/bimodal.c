/**
 * file bimodal.c
 * author Natalia Bolaños Murillo
 * Logic for Bimodal prediction
 **/

 // INCLUDES
#include "bimodal.h"

individual_result* bimodal(int* BHT, unsigned long instruction, char* outcome, int s, general_result* gen_result)
{
    // get index
    unsigned long index = get_index(instruction, s);

    // Store this branch results into struct
    individual_result* in_result = create_individual_result();
    in_result->outcome = outcome;
    in_result->instruction = instruction;

    // Use two bit counter predictor
    two_bit_counter(in_result, BHT, gen_result, index);
    return in_result;
}
