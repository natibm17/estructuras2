/**
 * file gshare.c
 * author Natalia Bolaños Murillo
 * Logic for Gshare prediction
 **/

 // INCLUDES
#include "gshare.h"


individual_result* gshare(int* BHT, unsigned long PC, char* outcome, int s,
    int gh, general_result* gen_result, unsigned long* history)
{
    // XOR between PC and history
    unsigned long gh_index = PC ^ *history;

    // Mask to get last s bits for indexing
    unsigned long mask = 0xFFFFFFFFFFFFFFFF;
    mask = mask >> (64-s);
    gh_index = gh_index & mask;

    // Store this branch results into struct
    individual_result* in_result = create_individual_result();
    in_result->instruction = PC;
    in_result->outcome = outcome;

    // Use two bit counter predictor
    two_bit_counter(in_result, BHT, gen_result, gh_index);

    // Update history register
    *history = update_history(*history, gh, outcome);

    return in_result;
}
