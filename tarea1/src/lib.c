
/**
 * file lib.c
 * author Natalia Bolaños Murillo
 * Shared fucntions for all predictors
 **/

 // INCLUDES
#include "lib.h"
#define BUFFER_SIZE 25

individual_result* create_individual_result()
{
    individual_result* res = malloc(sizeof(individual_result));
    res->instruction = 0;
    res->outcome = 0;
    res->prediction =  0;
    res->correct =  0;
    res-> predictor = 0;
    return res;
}

general_result* create_general_result()
{
    general_result* gen_res = malloc(sizeof(general_result));
    gen_res->correct_taken = 0;
    gen_res->incorrect_taken = 0;
    gen_res->correct_not_taken = 0;
    gen_res->incorrect_not_taken = 0;
    gen_res->correct_taken_t = 0;
    gen_res->incorrect_taken_t = 0;
    gen_res->correct_not_taken_t = 0;
    gen_res->incorrect_not_taken_t = 0;
    gen_res->percentage = 0.0;
    gen_res->total_branch = 0;
    gen_res-> prediction_type = malloc(25*sizeof(char));
    gen_res-> bht_size = 0;
    gen_res-> ph_size = 0;
    gen_res-> ph_size = 0;
    return gen_res;
}

void two_bit_counter(individual_result* in_result, int* BHT, general_result* gen_result, long index)
{
    // Read state
    int counter = BHT[index];

    // If the state is 00 or 01, prediction is Not Taken
    if (counter == 0 || counter == 1)
    {
        in_result->prediction = "N";
    }

    // If the state is 10 or 11, prediction is Taken
    else {
        in_result->prediction = "T";
    }

    // If outcome is taken, state is increased by one
    if(strcmp(in_result->outcome, "T") == 0 && BHT[index] != 3)
    {
        BHT[index] = counter+1;
    }

    // If outcome is taken, state is increased by one
    else if(strcmp(in_result->outcome, "N") == 0 && BHT[index] != 0)
    {
        BHT[index] = counter-1;
    }

    // If prediction and outcome are the same, the prediction was correct
    if (strcmp(in_result->prediction, in_result->outcome) == 0)
    {
        // Correct not taken and correct taken counters are increased
        in_result->correct = 1;
        if (strcmp(in_result->prediction, "N") == 0)
        {
            gen_result->correct_not_taken++;
        }
        else
        {
            gen_result->correct_taken++;
        }
    }

    // If prediction and outcome are the same, the prediction was incorrect
    else
    {
        // Inorrect not taken and incorrect taken counters are increased
        in_result->correct = 0;
        if (strcmp(in_result->prediction, "N") == 0)
        {
            gen_result->incorrect_not_taken++;
        }
        else
        {
            gen_result->incorrect_taken++;
        }
    }
}

unsigned long get_index(unsigned long PC, int s)
{
    // Mask is aplied to get last s bits used for indexing BHT
    unsigned long mask = 0xFFFFFFFFFFFFFFFF;
    mask = mask >> (64 - s);
    unsigned long index = mask & PC;
    return index;
}

unsigned long update_history(unsigned long history, int gh, char* outcome)
{
    // Shit register to the left
    unsigned long temp = history << 1;

    // If outcome is taken, 1 is added to the history
    if(strcmp(outcome, "T") == 0)
    {
        temp++;
    }

    // Mask is used to get the last gh bits of the register
    unsigned long mask = 0xFFFFFFFFFFFFFFFF;
    mask = mask >> (64 - gh);
    history = mask & temp;
    return history;
}
