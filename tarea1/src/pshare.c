/**
 * file pshare.c
 * author Natalia Bolaños Murillo
 * Logic for Pshare prediction
 **/

 // INCLUDES
#include "pshare.h"

individual_result* pshare(int* BHT, unsigned long* PHT, unsigned long PC, char* outcome, int s,
     int ph, general_result* gen_result)
{
    // get index
    unsigned long pht_index = get_index(PC, s);
    pht_index = (int)pht_index;

    // XOR between history and PC
    unsigned long index = pht_index ^ PHT[pht_index];

    // Store this branch results into struct
    individual_result* in_result = create_individual_result();
    in_result->instruction = PC;
    in_result->outcome = outcome;

    // Mask to get only the last s bits
    unsigned long mask = 0xFFFFFFFFFFFFFFFF;
    mask = mask >> (64-s);
    index = index & mask;

    // use two bit counter predictor
    two_bit_counter(in_result, BHT, gen_result, (int)index);

    // update history in BHT
    PHT[pht_index] = update_history(PHT[pht_index], ph, outcome);
    return in_result;
}
