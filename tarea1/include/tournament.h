/**
 * file tournament.h
 * author Natalia Bolaños Murillo
 * Logic for Tournament prediction
 **/

#ifndef _TOURNAMENT_H_
#define _TOURNAMENT_H_

// INCLUDES
#include "lib.h"
#include "pshare.h"
#include "gshare.h"
#include <string.h>

// FUNCIONS
 /*
 * Function : tournament
 *
 * Description : Tournament predictor logic
 *
 * Parameters : meta Metapredictor Table
                BHT  Branch Hisory table for pshare
                BHT  Branch Hisory table for gshare
                Instruction  Program Counter
                Outcome Branch outcome
                s   Number of bits for indexing
                ph   Number of bits phsare history register
                gh   Number of bits gshare history register
                gen_result Struct to save all results
                history gshare history register
 *
 * Return : Results for each branch
 */
individual_result* tournament(int* meta, int* BHT, int* BHT2, unsigned long* PHT, unsigned long PC,
    char* outcome, int s, int ph, int gh, general_result* gen_result, unsigned long* history);

/*
* Function : metapredictor
*
* Description : Metapredictor logic
*
* Parameters :  meta Metapredictor Table
                gh_res Gshare results
                ph_res Pshare results
                index metapredictor index
*
* Return : Predictor selected by the metapredictor
*/
int metapredictor(int* meta, individual_result* gh_res, individual_result* ph_res, long index);

#endif /* _TOURNAMENT_H_ */
