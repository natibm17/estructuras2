/**
 * file print.h
 * author Natalia Bolaños Murillo
 * Logic for printing results
 **/

#ifndef _PRINT_H_
#define _PRINT_H_

// INCLUDES
# include "lib.h"
#include <stdlib.h>
#include <stdio.h>

// FUNCIONS
 /*
 * Function : print_results
 *
 * Description : Prints results on terminal
 *
 * Parameters : results  General results struct
                bp Predictor type
 */
void print_results(general_result* results, int bp);

/*
* Function : get_results
*
* Description : Calculates final results for printing
*
* Parameters :  results  General results struct
                bp Predictor type
*/
void get_results(general_result* results, int bp);

/*
* Function : add_line
*
* Description : Add new line to the results file
*
* Parameters :  results  General results struct
                filename Nombre del archivo a escribir
                bp Predictor type
*/
void add_line(individual_result* results, char* filename, int bp);

/*
* Function : create_file
*
* Description : Creates results file
*
* Parameters :  filename Name of the file to be created
                bp Predictor type
*/
void create_file(char* filename, int bp);

#endif /* _PRINT_H_ */
