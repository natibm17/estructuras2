/**
 * file pshare.h
 * author Natalia Bolaños Murillo
 * Logic for Pshare prediction
 **/
#ifndef _PSHARE_H_
#define _PSHARE_H_

// INCLUDES
#include "lib.h"
#include "print.h"

// FUNCIONS
 /*
 * Function : pshare
 *
 * Description : Pshare predictor logic
 *
 * Parameters : BHT  Branch Hisory table
                PHT Patern History table
                PC  Program Counter
                Outcome Branch outcome
                s   Number of bits for indexing
                gen_result Struct to save all results
 *
 * Return : Results for each branch
 */
individual_result* pshare(int* BHT, unsigned long* PHT, unsigned long PC,
    char* outcome, int s, int ph, general_result* gen_result);
#endif /* _PSHARE_H_ */
