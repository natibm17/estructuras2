/**
 * file lib.h
 * author Natalia Bolaños Murillo
 * Shared functions for all predictors
 **/

#ifndef _LIB_H_
#define _LIB_H_

// INCLUDES
#include <stdlib.h>
#include <string.h>


// STRUCTS
/**
*   Individual results struct
*
**/
typedef struct
{
    unsigned long instruction;
    char* outcome;
    char* prediction;
    int correct;
    int predictor;
}individual_result;

/**
*   General results struct
*
**/
typedef struct
{
    unsigned int correct_taken;
    unsigned int incorrect_taken;
    unsigned int correct_not_taken;
    unsigned int incorrect_not_taken;
    unsigned int total_branch;
    unsigned int correct_taken_t;
    unsigned int incorrect_taken_t;
    unsigned int correct_not_taken_t;
    unsigned int incorrect_not_taken_t;
    double percentage;
    char* prediction_type;
    long bht_size;
    int ph_size;
    int gh_size;
}general_result;


// FUNCIONS
 /*
 * Function : create_individual_result
 *
 * Description : Create individual_result object
 *
 * Return : individual_result*
 */
individual_result* create_individual_result();

/*
* Function : create_general_result
*
* Description : Create general_result object
*
* Return : general_result*
*/
general_result* create_general_result();

/*
* Function : two_bit_counter
*
* Description : Two bit counter predictor logic
*
* Parameters : in_result    branch results struct
               BHT   Brand History Table
               gen_result   general results struct
               index    BHT index
*/
void two_bit_counter(individual_result* in_result, int* BHT, general_result* gen_result, long index);

/*
* Function : get_index
*
* Description : Calculates BHT index
*
* Parameters : PC   Program Counter
               s    Number of bits for indexing
 * Return : BHT index
*/
unsigned long get_index(unsigned long PC, int s);

/*
* Function : update_history
*
* Description : Updates history register
*
* Parameters : history    History register
               gh    Number of bits for gshare history register
               outcome    branch outcome
 * Return : Updated history register
*/
unsigned long update_history(unsigned long history, int gh, char* outcome);

#endif /* _LIB_H_ */
