/**
 * file pshare.h
 * author Natalia Bolaños Murillo
 * Logic for Pshare prediction
 **/

#ifndef _PARSE_H_
#define _PARSE_H_
#define BUFFER_SIZE 25

// INCLUDES
#include "lib.h"
#include "bimodal.h"
#include "print.h"
#include "pshare.h"
#include "gshare.h"
#include "tournament.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// STRUCTS
/*
* Data trace struct
*
*/
typedef struct
{
    unsigned long instruction;
    char* outcome;
}line_data;

// FUNCIONS
 /*
 * Function : parse
 *
 * Description : Parse input file
 *
 * Parameters : file  input file
                s   BHT size
                bp  Prediction type
                gh  Gshare register size
                ph  Pshare histroy register size
                o   Simulation output
 */
void parse(FILE* file, int s, int bp, int ph, int gh, int o);

 /*
 * Function : create_line_data
 *
 * Description : Create line_data object
 *
 * Return : line_data*
 */
line_data* create_line_data();

 /*
 * Function : analize_line
 *
 * Description : Read data trace
 *
 * Return : Line data trace
 */
line_data* analize_line(char* line);

#endif /* _PARSE_H_ */
