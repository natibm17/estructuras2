/**
 * file pshare.h
 * author Natalia Bolaños Murillo
 * Logic for Pshare prediction
 **/

#ifndef _BIMODAL_H_
#define _BIMODAL_H_

// INCLUDES
#include "lib.h"

// FUNCIONS
 /*
 * Function : bimodal
 *
 * Description : Bimodal predictor logic
 *
 * Parameters : BHT  Branch Hisory table
                Instruction  Program Counter
                Outcome Branch outcome
                s   Number of bits for indexing
                gen_result Struct to save all results
 *
 * Return : Results for each branch
 */
individual_result* bimodal(int* BHT, unsigned long instruction, char* outcome, int s, general_result* gen_result);
#endif /* _BIMODAL_H_ */
