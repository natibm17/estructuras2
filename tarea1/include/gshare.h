/**
 * file pshare.h
 * author Natalia Bolaños Murillo
 * Logic for Pshare prediction
 **/
#ifndef _GSHARE_H_
#define _GSHARE_H_

// INCLUDES
#include "lib.h"

// FUNCIONS
 /*
 * Function : gshare
 *
 * Description : Gshare predictor logic
 *
 * Parameters : BHT  Branch Hisory table
                PC  Program Counter
                Outcome Branch outcome
                s   Number of bits for indexing
                gh Number of bits on history register
                gen_result Struct to save all results
                history history register
 *
 * Return : Results for each branch
 */

individual_result* gshare(int* BHT, unsigned long PC, char* outcome, int s,
     int gh, general_result* gen_result, unsigned long* history);
#endif /* _GSHARE_H_ */
